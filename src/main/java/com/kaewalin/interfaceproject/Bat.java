/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kaewalin.interfaceproject;

/**
 *
 * @author ACER
 */
public class Bat extends Poultry {

    private String name;

    public Bat(String name) {
        super("Bat",2);
        this.name = name;
    }

    /**
     *
     */
    @Override
    public void fly() {
        System.out.println("Bat name is : " + name + " Fly at night ");
    }

    @Override
    public void eat() {
        System.out.println("Bat name is : " + name + " Can eat ");
    }

    @Override
    public void speak() {
        System.out.println("Bat name is : " + name + " Can speak");
    }

    @Override
    public void sleep() {
        System.out.println("Bat name is : " + name + " Can sleep");
    }

}
