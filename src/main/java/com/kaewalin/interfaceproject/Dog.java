/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kaewalin.interfaceproject;

/**
 *
 * @author ACER
 */
public class Dog extends LandAnimal {

    private String name;

    public Dog(String name) {
        super("Dog", 4);
        this.name = name;
    }

    /**
     *
     */
    @Override
    public void eat() {
        System.out.println("Dog name is : " + name + " Can eat");
    }

    @Override
    public void speak() {
        System.out.println("Dog name is : " + name + " speak >> box box !!!");
    }

    @Override
    public void sleep() {
        System.out.println("Dog name is : " + name + " Can sleep");
    }

    @Override
    public void run() {
        System.out.println("Dog: run");
        System.out.println("Dog name is : " + name + " Can run");
    }

}
