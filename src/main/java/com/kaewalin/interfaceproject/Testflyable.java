/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kaewalin.interfaceproject;

/**
 *
 * @author ACER
 */
public class Testflyable {

    public static void main(String[] args) {

        Bat bat = new Bat("Yujin");
        bat.fly();
        
        Plane plane = new Plane("Engine Number 1");
        plane.fly();

        Dog dog = new Dog("Tomboy");
        dog.run();

        Car car = new Car("Engine Number 2");
        car.run();

        Human human = new Human("Polo");
        human.run();

        Flyable[] flyable = {plane, bat};
        Runable[] runable = {dog, plane, car, human};
        
        System.out.println("----------------------------------------------------");
        System.out.println("Flyable");
        Flyable[] flyables = {bat, plane};
        for (int i = 0; i < runable.length; i++) {
            if (runable[i] instanceof Plane) {
                ((Plane) (runable[i])).startEngine();
                ((Plane) (runable[i])).raiseSpeed();
                ((Plane) (runable[i])).run();
                ((Plane) (runable[i])).fly();

            } else {
                runable[i].run();
            }
        }
        System.out.println("----------------------------------------------------");
        System.out.println("Flyable");
        for (int i = 0; i < flyable.length; i++) {
            if (flyable[i] instanceof Plane) {
                ((Plane) (flyable[i])).startEngine();
                ((Plane) (flyable[i])).raiseSpeed();
                ((Plane) (flyable[i])).run();
                ((Plane) (flyable[i])).fly();
            } else {
                flyable[i].fly();
            }
        }


    }  
}
